# Strike The Ban
This repository contains the code for the **StrikeTheBan** campaign website.

You can visit the site here: https://soc-fem.gitlab.io/striketheban2/ 

Anything you put in the `public/` directory will be served at the URL above.